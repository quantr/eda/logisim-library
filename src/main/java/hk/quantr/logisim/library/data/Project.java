/*
 * Copyright 2023 Peter <peter@quantr.hk>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package hk.quantr.logisim.library.data;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import java.util.ArrayList;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
@XStreamAlias("project")
public class Project {

	@XStreamAsAttribute
	public String source;

	@XStreamAsAttribute
	public String version;

	public Circuit circuit;
	public Main main;

	@XStreamImplicit(itemFieldName = "lib")
	public ArrayList<Library> libraries = new ArrayList<>();

	public Project(String source, String version, Circuit circuit) {
		this.source = source;
		this.version = version;
		this.circuit = circuit;
	}

}
