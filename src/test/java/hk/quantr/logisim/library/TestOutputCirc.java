package hk.quantr.logisim.library;

import com.thoughtworks.xstream.XStream;
import hk.quantr.logisim.library.data.A;
import hk.quantr.logisim.library.data.Circuit;
import hk.quantr.logisim.library.data.Component;
import hk.quantr.logisim.library.data.Library;
import hk.quantr.logisim.library.data.Location;
import hk.quantr.logisim.library.data.Main;
import hk.quantr.logisim.library.data.Project;
import hk.quantr.logisim.library.data.Tool;
import hk.quantr.logisim.library.data.Wire;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

/**
 *
 * @author Peter (peter@quantr.hk)
 */
public class TestOutputCirc {

	@Test
	public void test() throws IOException, IOException {
		XStream xstream = new XStream();
		xstream.processAnnotations(Project.class);

		Project project = new Project("3.8.0", "1.0", new Circuit("main"));
		project.main = new Main("main");
		Circuit circuit = project.circuit;
		Component comp = new Component("Pin", "0", new Location(180, 170));
		comp.a = new A("Component", "Component");
		circuit.components.add(comp);
		circuit.components.add(new Component("NOT Gate", "1", new Location(310, 170)));
		circuit.components.add(new Component("LED", "5", new Location(440, 110)));

		circuit.a.add(new A("appearance", "logisim_evolution"));
		circuit.a.add(new A("circuit", "main"));
		circuit.a.add(new A("circuitnamedboxfixedsize", "true"));
		circuit.a.add(new A("simulationFrequency", "2048.0"));

		circuit.wires.add(new Wire(new Location(180, 170), new Location(280, 170)));
		circuit.wires.add(new Wire(new Location(310, 170), new Location(370, 170)));
		circuit.wires.add(new Wire(new Location(370, 110), new Location(370, 170)));
		circuit.wires.add(new Wire(new Location(370, 110), new Location(440, 110)));

		Library lib0 = new Library("0", "#Wiring");
		lib0.tool = new Tool("Pin");
		lib0.tool.a = new A("appearance", "classic");
		project.libraries.add(lib0);
		project.libraries.add(new Library("1", "#Gates"));
		project.libraries.add(new Library("5", "#I/O"));
		project.libraries.add(new Library("8", "#Base"));

		String xml = xstream.toXML(project);
		System.out.println(xml);

		FileUtils.writeStringToFile(new File(System.getProperty("user.home") + "/Desktop/a.circ"), xml, "utf-8");
	}

}
