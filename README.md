A java library to export data structure into logisim project .circ file. Part of Quantr EDA tool.

# Simplest circ file

```
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<project source="3.8.0" version="1.0">
  <lib desc="#Wiring" name="0">
    <tool name="Pin">
      <a name="appearance" val="classic"/>
    </tool>
  </lib>
  <lib desc="#Gates" name="1"/>
  <lib desc="#I/O" name="5"/>
  <lib desc="#Base" name="8"/>
  <main name="main"/>
  <toolbar>
    <tool lib="8" name="Poke Tool"/>
    <tool lib="8" name="Edit Tool"/>
    <tool lib="8" name="Wiring Tool"/>
    <tool lib="8" name="Text Tool"/>
  </toolbar>
  <circuit name="main">
    <a name="appearance" val="logisim_evolution"/>
    <a name="circuit" val="main"/>
    <a name="circuitnamedboxfixedsize" val="true"/>
    <a name="simulationFrequency" val="2048.0"/>
    <comp lib="0" loc="(180,170)" name="Pin">
      <a name="appearance" val="classic"/>
    </comp>
    <comp lib="1" loc="(310,170)" name="NOT Gate"/>
    <comp lib="5" loc="(440,110)" name="LED"/>
    <wire from="(180,170)" to="(280,170)"/>
    <wire from="(310,170)" to="(370,170)"/>
    <wire from="(370,110)" to="(370,170)"/>
    <wire from="(370,110)" to="(440,110)"/>
  </circuit>
</project>
```

